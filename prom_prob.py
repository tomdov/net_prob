#!/usr/bin/python3

import prometheus_client as prom
import time
import json
from tcp_latency import measure_latency
from statistics import mean, median
from icmplib import ping, ICMPSocketError

req_summary = prom.Summary('netprob_sample', 'Metrics on processing a request')
def_timeout = 5 # seconds

def sec_to_msec(sec):
    return sec * 1000

def process_request_tcp(host):
    hostname = host["name"]

    samp  = measure_latency(host = host["host"], timeout=def_timeout, runs=runs_per_samp, port=80)
    sjson = {}

    if samp == None or samp == [None] or samp == []:
        # in case of error / timeout
        samp = [sec_to_msec(def_timeout)]
   
    #print(samp)

    sjson["host"] = hostname
    sjson["max"]  = round(max(samp), 2)
    sjson["min"]  = round(min(samp), 2)
    sjson["avg"]  = round(mean(samp), 2)
    sjson["med"]  = round(median(samp), 2)
    sjson["runs"] = runs_per_samp

    return sjson

def process_request_icmp(host):
    sjson = {}
    sjson["host"] = host["name"]
    sjson["runs"] = 1

    try:
        host_res = ping(host["host"], count=1, timeout=def_timeout, privileged=False)
    
        sjson["max"]  = host_res.max_rtt
        sjson["min"]  = host_res.min_rtt
        sjson["avg"]  = host_res.avg_rtt
    except ICMPSocketError as e:
        sjson["max"] = sjson["min"] = sjson["avg"] = sec_to_msec(def_timeout)
        print("Exception in icmp! " + str(e))

    return sjson

@req_summary.time()
def process_request(gauges):

    samp_json = {}

    for host in hosts:
        hostname = host["name"]
        
        if host["prob_type"] == "TCP":
            samp_json = process_request_tcp(host)
        elif host["prob_type"] == "icmp":
            samp_json = process_request_icmp(host)
        else:
            print("Type '" + host["prob_type"] + "' is not supported!")
            continue

        gauges[hostname].set(samp_json["max"])
   
        #print("Gauge set to {:}".format(samp_json["max"])) 
        #print(json.dumps(samp_json, indent=4))

 
with open("/netprob/config.json", 'r') as f:
    config = f.read()
    config = json.loads(config)

hosts         = config["hosts"]
runs_per_samp = config["run_per_host"]

if __name__ == '__main__':

    samp_counter = prom.Counter('netprob_my_counter', 'This is my counter')
   
    gauges = {}
 
    for host in hosts:
        hostname         = host["name"]
        gauges[hostname] = prom.Gauge('netprob_latency_' + hostname, 'This is my gauge')

    prom.start_http_server(54321)

    min_intv_samples_ms = 10000
 
    while True:
        start = time.time()
        samp_counter.inc(1)
       	process_request(gauges)

        left = min_intv_samples_ms - (time.time() - start) * 1000 
        
        if left > 0:
            time.sleep(float(left) / 1000)

