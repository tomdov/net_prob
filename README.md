NetProb is a Prometheus exporter made for probing endpoints included in the config.json file via TCP and ICMP.

Then Prometheus can export the data to the visualization tool of your choice.

1. change grafana-agent.yaml.example to grafana-agent.yaml and edit your endpoint

2. build the docker image:

docker build . -t gagent --network host

3. edit docker-compose.yml with the path to netprob's folder

4. run the container:

docker-compose up -d


