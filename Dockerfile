FROM grafana/agent:v0.43.4

RUN apt-get update && apt-get install -y python3 python3-pip
RUN apt-get update && apt-get install -y pipx
RUN mkdir -p ~/.config/pip/
RUN echo "[global]" >> ~/.config/pip/pip.conf
RUN echo "break-system-packages = true" >> ~/.config/pip/pip.conf

RUN pip3 install prometheus_client
RUN pip3 install tcp_latency
RUN pip3 install icmplib
 
ENTRYPOINT /netprob/grafana_start.sh

